import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int arr[] = new int[n];
		for (int arr_i = 0; arr_i < n; arr_i++) {
			arr[arr_i] = in.nextInt();
		}
		
		
		calculate(arr,n);
	}
	
	static void calculate(int arr[],int total){
		float plus=0;
		float minus=0;
		float zero=0;
		
		for(int i=0;i<total;i++){
			if(arr[i]>0){
				plus++;
			}else if(arr[i]<0){
				minus++;
			}
			else{
				zero++;
			}
		}
		System.out.println(String.format("%.6g%n", plus/total) + String.format("%.6g%n", minus/total) + String.format("%.6g%n", zero/total));
	}
}
