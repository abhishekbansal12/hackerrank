import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int counter = 1;

		for (int i = n; i > 0; i--) {
			for (int j = i - 1; j > 0; j--) {
				System.out.print(" ");
			}
			for (int k = 0; k < counter; k++) {
				System.out.print("#");

			}
			counter++;
			System.out.println("");

		}
	}
}
